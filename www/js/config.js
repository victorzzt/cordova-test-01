var maingame = maingame || {};

// Width height crossed as we are using it landscape

_config = () => {
  Object.defineProperty(maingame, "config", {
    value: {
      type: Phaser.AUTO,
      width: window.innerHeight,
      height: window.innerWidth,
      parent: 'gameparent',

      pixelArt: true,
      roundPixels: false,
      antialias: false,

      loader: {
        baseURL: 'assets'
      },
      scene: menuscene
    },
    writable: false // So that people don't break it
  });
}

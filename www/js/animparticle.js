class animparticle extends Phaser.GameObjects.Particles.Particle {
  constructor(emitter){
    super(emitter);
    this.t = 0;
    this.i = 0;
    this.animation = emitter.anim;
  }
  
  update(delta, step, processors){
    const result = super.update(delta, step, processors);
    
    this.t += delta;
    if( this.t >= this.animation.msPerFrame ){
      this.i += 1;
      if( this.i >= this.animation.frames.length ){
        this.i = 0;
      }
      this.frame = this.animation.frames[this.i].frame;
      this.t -= this.animation.msPerFrame;
    }
    return result;
  }
}
var maingame = maingame || {};

class menuscene extends Phaser.Scene {
  constructor() {
    super({
      key: 'menuScene'
    });
  }

  preload() {
    this.debugtouch = 1;
    this.canvaswidth = this.game.config.width;
    this.canvasheight = this.game.config.height;

    this.load.image('sky', 'Background01.png');

    this.load.spritesheet('spark01', 'sparktest01.png', {
      frameWidth: 256,
      frameHeight: 256
    });
    this.load.spritesheet('spark02', 'sparktest02.png', {
      frameWidth: 256,
      frameHeight: 256
    });
    this.load.spritesheet('spark03', 'sparktest03.png', {
      frameWidth: 256,
      frameHeight: 256
    });

    this.load.on('complete', () => {
      for (let i = 1; i <= 3; i++) {
        let thiskey = 'sp0' + i;
        let thisanim = 'spark0' + i;
        let thisanimkey = 'animsp0' + i;
        this[thisanimkey] = this.anims.create({
            key: thiskey,
            frames: this.anims.generateFrameNumbers(thisanim, {
              start: 0,
              end: 7
            }),
            frameRate: 10,
            repeat: -1
          });
      }
    });
  }

  create() {
    this.input.addPointer(10);

    this.add.image(this.canvaswidth / 2, this.canvasheight / 2, 'sky').setScale(2);

    // I'm alive
    this.rectw = this.canvasheight * 0.8;
    this.rect = this.add.graphics().setPosition(this.canvaswidth / 2, this.canvasheight / 2);
    this.rect.lineStyle(8, 0x004f4f, 1.0);
    this.rect.strokeRect(-this.rectw / 2, -this.rectw / 2, this.rectw, this.rectw)

    // TP helper
    this.tph = this.add.graphics();
    this.tphcols = [0xffffff, 0xff0000, 0x00ff00, 0x0000ff, 0xffff00, 0xff00ff, 0x00ffff, 0x7f7f00, 0x7f007f, 0x007f7f];

    // 10 emitters
    this.emitters = [];
    for (let i = 0; i < 10; i++) {
      let thisidx = Math.floor(Math.random() * 60000) % 3 + 1;
      var particles = this.add.particles('spark0' + thisidx);
      var emitter = particles.createEmitter({
          x: (this.canvaswidth / 11) * (i + 1),
          y: this.canvasheight / 2,
          frame: 0,
          quantity: 2,
          frequency: 20,
          speed: {
            start: 100,
            end: 30
          },
          lifespan: 1500,
          scale: {
            start: 0.2,
            end: 0.01
          },
          alpha: {
            start: 0.6,
            end: 0.01
          },
          particleClass: animparticle,
          blendMode: 'ADD',
          on: false
        });
      emitter.anim = this['animsp0' + thisidx]; // somehow data.set not working?
      this.emitters.push(emitter);
    }
  }

  update(timestep, dt) {
    this.rect.rotation += dt * 0.002;
    if (this.debugtouch) {
      // touch helper
      this.tph.clear();
      for (let i = 0; i < 10; i++) {
        let thisptr = i + 1;
        if (this.input['pointer' + thisptr].isDown) {
          this.tph.fillStyle(this.tphcols[i], 0.4);
          this.tph.fillCircle(this.input['pointer' + thisptr].x, this.input['pointer' + thisptr].y, 100);
        }
      }
    }

    for (let i = 0; i < 10; i++) {
      let thisptr = i + 1;
      let thisinput = this.input['pointer' + thisptr];

      if (thisinput.isDown) {
        if (!this.emitters[i].on) {
          this.emitters[i].start();
        }
        this.emitters[i].setPosition(thisinput.x, thisinput.y);
      } else {
        if (this.emitters[i].on) {
          this.emitters[i].stop();
        }
      }
    }
  }
}

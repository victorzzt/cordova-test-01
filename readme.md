#### A Cordova-Phaserio Test
* Performance pretty bad
    * The touch point will delay half a second
* Has the not native resolution
    * Work around now: 
    * `<meta name="viewport" content="user-scalable=no, initial-scale=0.5, minimum-scale=0.5, maximum-scale=0.5, width=device-width, viewport-fit=cover">`
    * But according to post on github, touch points will be off
